# subzero Config files by [ZERO](https://gitlab.com/zero.io)

JS configs for the zero blockchains.

## Install

Using npm:

```bash
npm i @zeroio/config
```

Using yarn:

```bash
yarn add @zeroio/config
```

## License

@zeroio/config is [GPL 3.0](./LICENSE) licensed.
